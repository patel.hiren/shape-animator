//import org.junit.Test;
//
//import java.util.LinkedHashMap;
//
//import cs3500.animator.controller.Controller;
//import cs3500.animator.model.Animation;
//import cs3500.animator.model.IModel;
//import cs3500.animator.model.Oval;
//import cs3500.animator.model.Transformation;
//import cs3500.animator.view.Composite;
//import cs3500.animator.view.IEditView;
//import cs3500.animator.view.SVGAnimationView;
//import cs3500.animator.view.TextView;
//
//import static junit.framework.Assert.assertNotNull;
//import static junit.framework.TestCase.assertEquals;
//
//public class AnimationModelTest {
//
//  @Test
//  public void testPrintMethod() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Transformation rt1 = new Transformation(1,10,200,200,
//            200,200,50,
//            50,100,100,255,255,0,0,
//            0,0);
//    Transformation rt2 = new Transformation(10,50,200,300,
//            200,300,50,
//            50,100,100,255,255,0,0,
//            0,0);
//    Transformation ot1 = new Transformation(6,20,440,440,70,
//            70,120,
//            120,60,60,0,0,0,0,255,
//            255);
//    Transformation ot2 = new Transformation(20,50,440,440,
//            70,250,120,
//            120,60,60,0,0,0,0,255,
//            255);
//    animation.addShape("rectangle","R");
//    animation.addShape("oval","O");
//    animation.addTransformationToShape("R", rt1);
//    animation.addTransformationToShape("R", rt2);
//    animation.addTransformationToShape("O", ot1);
//    animation.addTransformationToShape("O", ot2);
//
//    TextView view = new TextView(animation.getTimeline(), 0,0,500,
//            500);
//
//    assertEquals("canvas 0 0 500 500\n" +
//                    "shape R rectangle\n" +
//                    "motion R 1 200 200 50 100 255 0 0 10 200 200 50 100 255 0 0\n" +
//                    "motion R 10 200 200 50 100 255 0 0 50 300 300 50 100 255 0 0\n" +
//                    "shape O oval\n" +
//                    "motion O 6 440 70 120 60 0 0 255 20 440 70 120 60 0 0 255\n" +
//                    "motion O 20 440 70 120 60 0 0 255 50 440 250 120 60 0 0 255",
//            view.print().toString());
//  }
//
//  @Test (expected = IllegalArgumentException.class)
//  public void testInvalidTransformationNegTime() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Oval o = new Oval();
//    Transformation ot1 = new Transformation(1,-10,200,200,
//            200,200,50,
//            50,100,100,255,255,0,0,
//            0,0);
//  }
//
//  @Test (expected = IllegalArgumentException.class)
//  public void testInvalidTransformationInvalidTime() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Oval o = new Oval();
//    Transformation ot1 = new Transformation(15,10,200,200,
//            200,200,50,
//            50,100,100,255,255,0,0,
//            0,0);
//  }
//
//  @Test (expected = IllegalArgumentException.class)
//  public void testInvalidTransformationNegRed() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Oval o = new Oval();
//    Transformation ot1 = new Transformation(1,10,200,200,200,
//            200,50,
//            50,100,100,255,-255,0,0,
//            0,0);
//  }
//
//  @Test (expected = IllegalArgumentException.class)
//  public void testInvalidTransformationNegGreen() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Oval o = new Oval();
//    Transformation ot1 = new Transformation(1,10,200,200,200,
//            200,50,
//            50,100,100,255,255,0,-5,0,
//            0);
//  }
//
//  @Test (expected = IllegalArgumentException.class)
//  public void testInvalidTransformationNegBlue() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Oval o = new Oval();
//    Transformation ot1 = new Transformation(1,10,200,200,200,
//            200,50,
//            50,100,100,255,-255,0,0,0,
//            -10);
//  }
//
//  @Test
//  public void testSVGOutput() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Transformation rt1 = new Transformation(1,10,200,200,200,
//            200,50,
//            50,100,100,255,255,0,0,0,
//            0);
//    Transformation rt2 = new Transformation(10,50,200,300,200,
//            300,50,
//            50,100,100,255,255,0,0,0,
//            0);
//    Transformation ot1 = new Transformation(6,20,440,440,70,
//            70,120,
//            120,60,60,0,0,0,0,255,
//            255);
//    Transformation ot2 = new Transformation(20,50,440,440,70,
//            250,120,
//            120,60,60,0,0,0,0,255,
//            255);
//    animation.addShape("rectangle","R");
//    animation.addShape("oval","O");
//    animation.addTransformationToShape("R", rt1);
//    animation.addTransformationToShape("R", rt2);
//    animation.addTransformationToShape("O", ot1);
//    animation.addTransformationToShape("O", ot2);
//
//    SVGAnimationView view = new SVGAnimationView(animation.getTimeline(), 0,0,
//            500,500);
//
//    assertEquals("<svg width=\"500\" height=\"500\" version=\"1.1\"\n" +
//                    "xmlns=\"http://www.w3.org/2000/svg\">\n<rect id=\"R\" x=\"200\" y=\"200\"" +
//                    " width=\"50\" height=\"100\" fill=\"rgb(255,0,0)\" opacity=\"0\" >\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"0ms\" attributeName=" +
//                    "\"opacity\" from=\"0\" to=\"1\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"19900ms\" attributeName" +
//                    "=\"opacity\" from=\"1\" to=\"1\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"900ms\" attribute" +
//                    "Name=\"x\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"900ms\" attribute" +
//                    "Name=\"y\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"900ms\" attribute" +
//                    "Name=\"width\" from=\"50\" to=\"50\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"900ms\" attribute" +
//                    "Name=\"height\" from=\"100\" to=\"100\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"100ms\" dur=\"900ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(255,0,0)\" to=\"rgb(255,0,0)\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"4000ms\" attribute" +
//                    "Name=\"x\" from=\"200\" to=\"300\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"4000ms\" attribute" +
//                    "Name=\"y\" from=\"200\" to=\"300\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"4000ms\" attribute" +
//                    "Name=\"width\" from=\"50\" to=\"50\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"4000ms\" attribute" +
//                    "Name=\"height\" from=\"100\" to=\"100\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"4000ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(255,0,0)\" to=\"rgb(255,0,0)\" fill=\"freeze\" />\n" +
//                    "</rect>\n" +
//                    "<ellipse id=\"O\" cx=\"440\" cy=\"70\" rx=\"60\" ry=\"30\" fill=\"rgb" +
//                    "(0,0,255)\" opacity=\"0\" >\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"0ms\" attribute" +
//                    "Name=\"opacity\" from=\"0\" to=\"1\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"19400ms\" attribute" +
//                    "Name=\"opacity\" from=\"1\" to=\"1\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"1400ms\" attribute" +
//                    "Name=\"cx\" from=\"440\" to=\"440\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"1400ms\" attribute" +
//                    "Name=\"cy\" from=\"70\" to=\"70\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"1400ms\" attribute" +
//                    "Name=\"rx\" from=\"60\" to=\"60\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"1400ms\" attribute" +
//                    "Name=\"ry\" from=\"30\" to=\"30\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"600ms\" dur=\"1400ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(0,0,255)\" to=\"rgb(0,0,255)\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"2000ms\" dur=\"3000ms\" attribute" +
//                    "Name=\"cx\" from=\"440\" to=\"440\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"2000ms\" dur=\"3000ms\" attribute" +
//                    "Name=\"cy\" from=\"70\" to=\"250\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"2000ms\" dur=\"3000ms\" attribute" +
//                    "Name=\"rx\" from=\"60\" to=\"60\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"2000ms\" dur=\"3000ms\" attribute" +
//                    "Name=\"ry\" from=\"30\" to=\"30\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"2000ms\" dur=\"3000ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(0,0,255)\" to=\"rgb(0,0,255)\" fill=\"freeze\" />\n" +
//                    "</ellipse>\n" +
//                    "</svg>",
//            view.print().toString());
//  }
//
//  @Test
//  public void testSVGOutputBuilding() {
//    Animation animation = new Animation(new LinkedHashMap<>(),0,0,500,
//            500);
//    Transformation rt1 = new Transformation(0,5,200,200,200,
//            200,50,
//            50,100,100,0,255,0,0,0,
//            0);
//    Transformation rt2 = new Transformation(5,10,200,200,200,
//            200,50,
//            50,100,100,255,0,0,255,0,
//            0);
//    Transformation rt3 = new Transformation(10,15,200,200,200,
//            200,50,
//            50,100,100,0,0,255,0,0,
//            255);
//    animation.addShape("rectangle","R");
//    animation.addTransformationToShape("R", rt1);
//    animation.addTransformationToShape("R", rt2);
//    animation.addTransformationToShape("R", rt3);
//
//    SVGAnimationView view = new SVGAnimationView(animation.getTimeline(), 0,0,
//            500,500);
//
//    assertEquals("<svg width=\"500\" height=\"500\" version=\"1.1\"\n" +
//                    "xmlns=\"http://www.w3.org/2000/svg\">\n<rect id=\"R\" x=\"200\" y=\"200\" " +
//                    "width=\"50\" height=\"100\" fill=\"rgb(0,0,0)\" opacity=\"0\" >\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"0ms\" attribute" +
//                    "Name=\"opacity\" from=\"0\" to=\"1\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"20000ms\" attribute" +
//                    "Name=\"opacity\" from=\"1\" to=\"1\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"500ms\" attribute" +
//                    "Name=\"x\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"500ms\" attribute" +
//                    "Name=\"y\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"500ms\" attribute" +
//                    "Name=\"width\" from=\"50\" to=\"50\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"500ms\" attribute" +
//                    "Name=\"height\" from=\"100\" to=\"100\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"0ms\" dur=\"500ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(0,0,0)\" to=\"rgb(255,0,0)\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"500ms\" dur=\"500ms\" attribute" +
//                    "Name=\"x\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"500ms\" dur=\"500ms\" attribute" +
//                    "Name=\"y\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"500ms\" dur=\"500ms\" attribute" +
//                    "Name=\"width\" from=\"50\" to=\"50\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"500ms\" dur=\"500ms\" attribute" +
//                    "Name=\"height\" from=\"100\" to=\"100\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"500ms\" dur=\"500ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(255,0,0)\" to=\"rgb(0,255,0)\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"500ms\" attribute" +
//                    "Name=\"x\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"500ms\" attribute" +
//                    "Name=\"y\" from=\"200\" to=\"200\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"500ms\" attribute" +
//                    "Name=\"width\" from=\"50\" to=\"50\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"500ms\" attribute" +
//                    "Name=\"height\" from=\"100\" to=\"100\" fill=\"freeze\" />\n" +
//                    "<animate attributeType=\"xml\" begin=\"1000ms\" dur=\"500ms\" attribute" +
//                    "Name=\"fill\" from=\"rgb(0,255,0)\" to=\"rgb(0,0,255)\" fill=\"freeze\" />\n" +
//                    "</rect>\n" +
//                    "</svg>",
//            view.print().toString());
//  }
//
//  @Test
//  public void testController() {
//
//    IModel m = new Animation(new LinkedHashMap<>(), 0,0,500,500);
//    IEditView v = new Composite(m.getTimeline(), m.getMinX(), m.getMinY(), m.getSceneWidth(),
//            m.getSceneHeight(), 50);
//    Controller c = new Controller(m, v);
//    assertNotNull(c);
//  }
//
//}
