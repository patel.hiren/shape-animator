package cs3500.animator.controller;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;

import javax.swing.JOptionPane;

import cs3500.animator.model.Animation;
import cs3500.animator.model.IModel;
import cs3500.animator.model.IShape;
import cs3500.animator.model.Keyframe;
import cs3500.animator.util.AnimationReader;
import cs3500.animator.view.Composite;
import cs3500.animator.view.IEditView;
import cs3500.animator.view.SVGAnimationView;
import cs3500.animator.view.TextView;

public class Controller implements IActionListener {

  private IModel model;
  private IEditView view;

  /**
   * Constructor assigns model and view, and sets this controller as view's listener.
   *
   * @param model model
   * @param view view
   */
  public Controller(IModel model, IEditView view) {
    this.model = model;
    this.view = view;
    this.view.setListener(this);
  }

  @Override
  public void action(String description) {
    switch (description) {
      case "play":
        view.startTimer();
        break;
      case "pause":
        view.stopTimer();
        break;
      case "restart":
        view.restartTimer();
        break;
      case "enable looping":
        view.enableLooping(true);
        break;
      case "disable looping":
        view.enableLooping(false);
        break;
      case "increase delay":
        view.changeDelay(50);
        break;
      case "normal delay":
        view.resetDelay();
        break;
      case "decrease delay":
        view.changeDelay(-50);
        break;
      case "add shape":
        String[] options = new String[]{"rectangle", "oval"};
        String typeOfShape = (String) JOptionPane.showInputDialog(null,
                "Pick type of shape", "Pick Shape Type",
                JOptionPane.QUESTION_MESSAGE, null,
                options, // Array of choices
                options[1]); // Initial choice
        String nameOfShape = JOptionPane.showInputDialog("Name of shape: ");
        while (nameOfShape.equalsIgnoreCase("")
                || duplicateName(nameOfShape)) {
          nameOfShape = JOptionPane.showInputDialog("Please enter valid name of shape: ");
        }
        model.addShape(view.getLayerSelected(), typeOfShape, nameOfShape);
        view.updateTimeline(model.getTimeline());
        view.clearDefaultShapeListModel();
        view.clearDefaultShapeListModel();
        for (String namesOfShape : model.getTimeline().get(view.getLayerSelected()).keySet()) {
          view.addShapeToDefaultShapeListModel(namesOfShape);
        }
        view.restartTimer();
        break;
      case "add keyframe":
        if (view.getShapeSelected() != null) {
          int newKfTime = Integer.parseInt(JOptionPane.showInputDialog("Input time for " +
                  "keyframe:"));
          model.addKeyframeToShape(view.getLayerSelected(), view.getShapeSelected(), newKfTime);
          view.updateTimeline(model.getTimeline());
          view.clearDefaultKeyframeListModel();
          for (Keyframe kf :
                  model.getTimeline().get(view.getLayerSelected()).get(view.getShapeSelected())
                          .getKeyframes()) {
            view.addKeyframeToDefaultKeyframeListModel(
                    model.getTimeline().get(view.getLayerSelected()).get(
                            view.getShapeSelected()).getKeyframes().indexOf(kf) + 1);
          }
          view.restartTimer();
        }
        break;
      case "delete shape":
        view.clearDefaultKeyframeListModel();
        model.removeShape(view.getLayerSelected(), view.getShapeSelected());
        view.updateTimeline(model.getTimeline());
        view.clearDefaultShapeListModel();
        for (String namesOfShape : model.getTimeline().get(view.getLayerSelected()).keySet()) {
          view.addShapeToDefaultShapeListModel(namesOfShape);
        }
        view.restartTimer();
        break;
      case "set":
        try {
          int newTime = Integer.parseInt(view.getKeyframeTimeText());
          int newX = Integer.parseInt(view.getKeyframeXText());
          int newY = Integer.parseInt(view.getKeyframeYText());
          int newW = Integer.parseInt(view.getKeyframeWText());
          int newH = Integer.parseInt(view.getKeyframeHText());
          int newR = Integer.parseInt(view.getKeyframeRText());
          int newG = Integer.parseInt(view.getKeyframeGText());
          int newB = Integer.parseInt(view.getKeyframeBText());
          int newRotation = Integer.parseInt(view.getKeyframeRotationText());

          model.updateKeyframeInShape(view.getLayerSelected(), view.getShapeSelected(),
                  view.getKeyframeSelectedNum(), newTime, newX, newY, newW, newH, newR, newG,
                  newB, newRotation);
          view.updateTimeline(model.getTimeline());
          view.restartTimer();
        } catch (NumberFormatException n) {
          throw new IllegalArgumentException("Please input valid number.");
        }
        break;
      case "delete keyframe":
        if (model.getTimeline().get(view.getLayerSelected())
                .get(view.getShapeSelected()).getKeyframes().size() > 0) {
          model.removeKeyframeFromShape(view.getLayerSelected(), view.getShapeSelected(),
                  view.getKeyframeSelectedNum());
          view.updateTimeline(model.getTimeline());
          view.clearDefaultKeyframeListModel();
          for (Keyframe kf : model.getTimeline().get(view.getLayerSelected())
                  .get(view.getShapeSelected()).getKeyframes()) {
            view.addKeyframeToDefaultKeyframeListModel(model.getTimeline()
                    .get(view.getLayerSelected()).get(view.getShapeSelected())
                    .getKeyframes().indexOf(kf) + 1);
          }
          view.restartTimer();
        }
        break;
      case "save" :
        String[] options2 = new String[]{"text", "svg"};
        String saveAs = (String) JOptionPane.showInputDialog(null,
                "Pick type of save", "Save As",
                JOptionPane.QUESTION_MESSAGE, null,
                options2, // Array of choices
                options2[0]); // Initial choice
        if (saveAs.equalsIgnoreCase("text")) {
          try {
            String fileNameTxt = JOptionPane.showInputDialog("Name of file: ");
            TextView temp2 = new TextView(model.getTimeline(), model.getMinX(),
                    model.getMinY(), model.getSceneWidth(), model.getSceneHeight());
            FileWriter fw = new FileWriter(fileNameTxt + ".txt");
            fw.append(temp2.print().toString());
            fw.close();
          } catch (IOException y) {
            throw new IllegalArgumentException("FileWriter in Controller did not work");
          }
        } else if (saveAs.equalsIgnoreCase("svg")) {
          try {
            String fileName = JOptionPane.showInputDialog("Name of file: ");
            SVGAnimationView temp = new SVGAnimationView(model.getTimeline(), model.getMinX(),
                    model.getMinY(), model.getSceneWidth(), model.getSceneHeight());
            FileWriter fw = new FileWriter(fileName + ".svg");
            fw.append(temp.print().toString());
            fw.close();
          } catch (IOException x) {
            throw new IllegalArgumentException("FileWriter in Controller did not work");
          }
        }
        break;
      case "load" :
        try {
          String fileNameLoad = JOptionPane.showInputDialog("Name of file: ");
          Readable r = new FileReader(fileNameLoad);
          Animation.Builder b = new Animation.Builder();
          IModel newModel = AnimationReader.parseFile(r, b);
          view.setVisibility(false);
          IEditView newView = new Composite(newModel.getTimeline(), newModel.getMinX(),
                  newModel.getMinY(), newModel.getSceneWidth(), newModel.getSceneHeight(),
                  50);
          Controller newC = new Controller(newModel, newView);
        } catch (IOException z) {
          throw new IllegalArgumentException("FileReader in controller didn't work");
        }
        break;
      case "add layer" :
        model.addLayer();
        view.updateTimeline(model.getTimeline());
        view.clearDefaultLayerListModel();
        for (int i = 0; i < model.getTimeline().size(); i++) {
          view.addIndexToDefaultLayerListModel(i);
        }
        view.restartTimer();
        break;
      case "swap layer" :
        try {
          int layerOne = Integer.parseInt(JOptionPane.showInputDialog("Layer 1: "));
          int layerTwo = Integer.parseInt(JOptionPane.showInputDialog("Layer 2: "));
          model.swapLayers(layerOne - 1, layerTwo - 1);
          view.updateTimeline(model.getTimeline());
          view.clearDefaultLayerListModel();
          for (int i = 0; i < model.getTimeline().size(); i++) {
            view.addIndexToDefaultLayerListModel(i);
          }
          view.restartTimer();
        } catch (NumberFormatException l) {
          throw new IllegalArgumentException("Please input valid numbers");
        }
        break;
      case "delete layer" :
        model.deleteLayer(view.getLayerSelected());
        view.updateTimeline(model.getTimeline());
        view.clearDefaultLayerListModel();
        for (int i = 0; i < model.getTimeline().size(); i++) {
          view.addIndexToDefaultLayerListModel(i);
        }
        view.restartTimer();
        break;
      default:
        break;
    }
  }

  private boolean duplicateName(String name) {
    for (LinkedHashMap<String, IShape> hm : model.getTimeline()) {
      if (hm.keySet().contains(name)) {
        return true;
      }
    }
    return false;
  }
}