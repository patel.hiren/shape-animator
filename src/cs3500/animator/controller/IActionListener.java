package cs3500.animator.controller;

public interface IActionListener {

  /**
   * Performs the appropriate changes to the view and model after a button is pressed.
   *
   * @param description String representing the button pressed
   */
  void action(String description);

}