package cs3500.animator;


import java.io.*;

import cs3500.animator.controller.Controller;
import cs3500.animator.model.Animation;
import cs3500.animator.model.IModel;
import cs3500.animator.util.AnimationReader;
import cs3500.animator.view.Composite;
import cs3500.animator.view.IEditView;
import cs3500.animator.view.SVGAnimationView;
import cs3500.animator.view.TextView;
import cs3500.animator.view.VisualAnimation;

/**
 * Main class parses program arguments to run the specified file in the specified view, with
 * optional arguments to customize speed and an output file.
 */
public class MainAnimation {
  /**
   * The main method.
   * @param args the args
   */
  public static void main(String[] args) {

    int argsPosition = -1;
    String view = "edit";
    String in = "shape-animator/buildings.txt";
    String out = "Regular Out";
    int speed = 1;

    for (String s : args) {
      argsPosition++;
      if (s.equalsIgnoreCase("-view")) {
        view = args[argsPosition + 1];
      } else if (s.equalsIgnoreCase("-in")) {
        in = args[argsPosition + 1];
      } else if (s.equalsIgnoreCase("-out")) {
        out = args[argsPosition + 1];
      } else if (s.equalsIgnoreCase("-speed")) {
        try {
          speed = Integer.parseInt(args[argsPosition + 1]);
        } catch (NumberFormatException e) {
          throw new IllegalArgumentException("Please input valid number after speed.");
        }
      }
    }

    if (view.equalsIgnoreCase("not valid view")) {
      throw new IllegalArgumentException("Please input valid view.");
    } else if (in.equalsIgnoreCase("not valid in")) {
      throw new IllegalArgumentException("Please input valid input file.");
    } else if (speed < 0) {
      throw new IllegalArgumentException("Please input a non-negative input value.");
    }

    if (view.equalsIgnoreCase("text")) {
      // For the input.
      try {
        Readable r = new FileReader(in);
        Animation.Builder b = new Animation.Builder();
        IModel ar = AnimationReader.parseFile(r, b);
        TextView tv = new TextView(ar.getTimeline(), ar.getMinX(), ar.getMinY(),
                ar.getSceneWidth(), ar.getSceneHeight());
        if (!(out.equalsIgnoreCase("regular out"))) {
          try {
            FileWriter fw = new FileWriter(out);
            fw.append(tv.print().toString());
            fw.close();
          } catch (IOException e) {
            throw new IllegalArgumentException("FileWriter did not work");
          }
        } else {
          System.out.print(tv.print());
        }
      } catch (FileNotFoundException f) {
        throw new IllegalArgumentException("Not a valid file");
      }
    } else if (view.equalsIgnoreCase("visual")) {
      try {
        Readable r = new FileReader(in);
        Animation.Builder b = new Animation.Builder();
        IModel ar = AnimationReader.parseFile(r, b);
        new VisualAnimation(ar.getTimeline(), ar.getMinX(), ar.getMinY(),
                ar.getSceneWidth(), ar.getSceneHeight(), speed);
      } catch (FileNotFoundException f) {
        throw new IllegalArgumentException("Not a valid file");
      }
    } else if (view.equalsIgnoreCase("svg")) {
      try {
        Readable r = new FileReader(in);
        Animation.Builder b = new Animation.Builder();
        IModel ar = AnimationReader.parseFile(r, b);
        SVGAnimationView tv = new SVGAnimationView(ar.getTimeline(), ar.getMinX(), ar.getMinY(),
                ar.getSceneWidth(), ar.getSceneHeight());
        if (!(out.equalsIgnoreCase("regular out"))) {
          try {
            FileWriter fw = new FileWriter(out);
            fw.append(tv.print().toString());
            fw.close();
          } catch (IOException e) {
            throw new IllegalArgumentException("FileWriter did not work");
          }
        } else {
          System.out.print(tv.print());
        }
      } catch (FileNotFoundException f) {
        throw new IllegalArgumentException("Not a valid file");
      }
    } else if (view.equalsIgnoreCase("edit")) {
      try {
        Readable r = new FileReader(in);
        Animation.Builder b = new Animation.Builder();
        IModel ar = AnimationReader.parseFile(r, b);
        IEditView v = new Composite(ar.getTimeline(), ar.getMinX(), ar.getMinY(),
                ar.getSceneWidth(), ar.getSceneHeight(), speed);
        Controller c = new Controller(ar, v);
      } catch (FileNotFoundException f) {
        throw new IllegalArgumentException("Not a valid file");
      }
    } else {
      throw new IllegalArgumentException("No Valid Input Found");
    }
  }
}