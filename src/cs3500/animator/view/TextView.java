package cs3500.animator.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import cs3500.animator.model.IShape;
import cs3500.animator.model.Transformation;

/** The view for outputting a text description. */
public class TextView extends AbstractView {

  public TextView(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX, int minY,
                  int sceneWidth,
                  int sceneHeight) {
    super(timeline, minX, minY, sceneWidth, sceneHeight);
  }

  @Override
  public Appendable print() {
    StringBuilder sb = new StringBuilder();
    int shapeIndex = 0;
    sb.append("canvas " + minX + " " + minY + " " + sceneWidth + " " + sceneHeight + "\n");
    for (LinkedHashMap<String, IShape> hm : timeline) {
      for (Map.Entry<String, IShape> entry : hm.entrySet()) {
        String name = entry.getKey();
        IShape s = entry.getValue();
        sb.append("shape " + name + " " + s.figureOutShape() + "\n");
        int transformationIndex = 0;
        for (Transformation t : s.getTransformations()) {
          sb.append("motion " + name + " "
                  + t.getStartTime() + " "
                  + t.getStartX() + " "
                  + t.getStartY() + " " + t.getStartWidth() + " "
                  + t.getStartHeight() + " " + t.getStartR() + " "
                  + t.getStartG() + " " + t.getStartB() + " "
                  + t.getEndTime() + " " + t.getEndX() + " "
                  + t.getEndY() + " " + t.getEndWidth() + " "
                  + t.getEndHeight() + " " + t.getEndR() + " "
                  + t.getEndG() + " " + t.getEndB());
          if (!(shapeIndex == timeline.size() - 1 && transformationIndex ==
                  s.getTransformations().size() - 1)) {
            sb.append("\n");
          }
          transformationIndex++;
        }
        shapeIndex++;
      }
    }
    return sb;
  }
}