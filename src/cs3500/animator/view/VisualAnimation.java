package cs3500.animator.view;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.JFrame;
import javax.swing.Timer;

import cs3500.animator.model.IShape;

/** The view for displaying the animation visually. */
public class VisualAnimation extends JFrame implements IView {

  /**
   * Constructor.
   * @param timeline the timeline
   * @param minX the minx
   * @param minY the miny
   * @param sceneWidth the width
   * @param sceneHeight the height
   * @param speed the speed
   */
  public VisualAnimation(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX, int minY,
                         int sceneWidth,
                         int sceneHeight, int speed) {
    super();
    this.setTitle("Shape Animator!");
    this.setVisible(true);
    this.setSize(sceneWidth,sceneHeight);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    // Overlaying the Panel on this Frame.
    VisualPanel visualPanel = new VisualPanel(timeline, minX, minY, sceneWidth, sceneHeight);
    visualPanel.setLayout(new BorderLayout());
    this.add(visualPanel, BorderLayout.CENTER);

    // Initializing the Timer.
    this.pack();
    new Timer(1000 / speed, visualPanel).start();
  }

  @Override
  public Appendable print() {
    throw new UnsupportedOperationException("Visual doesn't need a print method.");
  }
}