package cs3500.animator.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import cs3500.animator.model.IShape;

/** The view for outputting an SVG file. */
public class SVGAnimationView extends AbstractView {

  public SVGAnimationView(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX, int minY,
                          int sceneWidth,
                          int sceneHeight) {
    super(timeline, minX, minY, sceneWidth, sceneHeight);
  }

  @Override
  public Appendable print() {
    StringBuilder fw = new StringBuilder();
    fw.append("<svg width=\"" + sceneWidth + "\" height=\"" + sceneHeight + "\" version=\"1" +
            ".1\"\nxmlns=\"" + "http://www.w3.org/2000/svg\">\n");
    for (LinkedHashMap<String, IShape> hm : timeline) {
      for (Map.Entry<String, IShape> entry : hm.entrySet()) {
        String key = entry.getKey();
        IShape s = entry.getValue();
        fw.append(s.svgText(key, minX, minY));
      }
    }
    fw.append("</svg>");
    return fw;
  }

}