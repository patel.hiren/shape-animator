package cs3500.animator.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.JScrollPane;
import javax.swing.DefaultListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cs3500.animator.controller.IActionListener;
import cs3500.animator.model.IShape;
import cs3500.animator.model.Keyframe;

/** The Editing visual animation view. */
//public class Composite extends JFrame implements IView {
public class Composite extends JFrame implements IEditView {

  private VisualPanel visualPanel;
  private ArrayList<LinkedHashMap<String, IShape>> timeline;
  private JButton startButton;
  private JButton pauseButton;
  private JButton restartButton;
  private JButton loopEnableButton;
  private JButton loopDisableButton;
  private JButton increaseDelayButton;
  private JButton normalDelayButton;
  private JButton decreaseDelayButton;
  private JButton addShapeButton;
  private JButton addKeyframeButton;
  private JButton deleteShapeButton;
  private JButton setButton;
  private JButton deleteKeyframeButton;
  private JButton saveButton;
  private JButton loadButton;
  private JButton addLayer;
  private JButton swapLayer;
  private JButton deleteLayer;
  private JTextField keyframeTimeText;
  private JTextField keyframeXText;
  private JTextField keyframeYText;
  private JTextField keyframeWText;
  private JTextField keyframeHText;
  private JTextField keyframeRText;
  private JTextField keyframeGText;
  private JTextField keyframeBText;
  private JTextField keyframeRotationText;
  private Keyframe selected;
  private DefaultListModel defaultLayerList;
  private DefaultListModel defaultShapeListModel;
  private DefaultListModel defaultKeyframeListModel;
  private JList<Integer> layerList;
  private JList<String> shapeList;
  private JList<Keyframe> keyframeList;
  private int layerSelected;
  private String shapeSelected;
  private int keyframeSelectedNum;
  private int delay;
  private Timer timer;
  private Scrollbar scrubb;


  /**
   * Constructor adds all the panels to the frame, starts the timer. Every tick of the timer,
   * the visualPanel is repainted. Every button, when pressed, refers to the controller to
   * make the appropriate changes to the view and model.
   *
   * @param timeline    the timeline
   * @param minX        the minx
   * @param minY        the miny
   * @param sceneWidth  the width
   * @param sceneHeight the height
   * @param speed       the speed (used to calculate delay in timer)
   */
  public Composite(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX, int minY,
                   int sceneWidth,
                   int sceneHeight, int speed) {
    super();
    this.setTitle("Shape Animator!");
    this.setVisible(true);
    this.setSize(sceneWidth, sceneHeight);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.delay = 1000 / speed;
    this.timeline = timeline;


    // Overlaying the Panel on this Frame.
    visualPanel = new VisualPanel(timeline, minX, minY, sceneWidth, sceneHeight);
    visualPanel.setLayout(new BorderLayout());
    this.add(visualPanel, BorderLayout.CENTER);

    // creating all the panels.
    JPanel overallPanel = new JPanel(new GridLayout(5, 0));
    JPanel buttonPanel = new JPanel(new GridLayout(2, 0));
    JPanel listPanel = new JPanel(new GridLayout());
    JPanel keyframePanel = new JPanel(new GridLayout(2, 0));
    JPanel keyframeButtonPanel = new JPanel(new GridLayout());

    // Puts the button panel in the frame.
    // Adds actions to the buttons.
    startButton = new JButton("play");
    startButton.setActionCommand("play");

    pauseButton = new JButton("pause");
    pauseButton.setActionCommand("pause");

    restartButton = new JButton("restart");
    restartButton.setActionCommand("restart");

    loopEnableButton = new JButton("enable looping");
    loopEnableButton.setActionCommand("enable looping");

    loopDisableButton = new JButton("disable looping");
    loopDisableButton.setActionCommand("disable looping");

    increaseDelayButton = new JButton("increase delay");
    increaseDelayButton.setActionCommand("increase delay");

    normalDelayButton = new JButton("normal delay");
    normalDelayButton.setActionCommand("normal delay");

    decreaseDelayButton = new JButton("decrease delay");
    decreaseDelayButton.setActionCommand("decrease delay");

    addShapeButton = new JButton("add shape");
    addShapeButton.setActionCommand("add shape");

    addKeyframeButton = new JButton("add keyframe");
    addKeyframeButton.setActionCommand("add keyframe");

    saveButton = new JButton("save");
    saveButton.setActionCommand("save");

    loadButton = new JButton("load");
    loadButton.setActionCommand("load");

    addLayer = new JButton("add layer");
    addLayer.setActionCommand("add layer");

    swapLayer = new JButton("swap layer");
    swapLayer.setActionCommand("swap layer");

    deleteLayer = new JButton("delete layer");
    deleteLayer.setActionCommand("delete layer");

    // creates more buttons and adds them to the panel.
    deleteShapeButton = new JButton("delete shape");
    deleteShapeButton.setActionCommand("delete shape");
    keyframeButtonPanel.add(deleteShapeButton);

    setButton = new JButton("set");
    setButton.setActionCommand("set");
    keyframeButtonPanel.add(setButton);

    deleteKeyframeButton = new JButton("delete keyframe");
    deleteKeyframeButton.setActionCommand("delete keyframe");
    keyframeButtonPanel.add(deleteKeyframeButton);

    // creates labels for text fields.
    JLabel keyframeBlankTextLabel = new JLabel("");
    JLabel keyframeBlankTextLabel2 = new JLabel("");
    JLabel keyframeTimeTextLabel = new JLabel("time: ");
    JLabel keyframeXTextLabel = new JLabel("x: ");
    JLabel keyframeYTextLabel = new JLabel("y: ");
    JLabel keyframeWTextLabel = new JLabel("w: ");
    JLabel keyframeHTextLabel = new JLabel("h: ");
    JLabel keyframeRTextLabel = new JLabel("r: ");
    JLabel keyframeGTextLabel = new JLabel("g: ");
    JLabel keyframeBTextLabel = new JLabel("b: ");
    JLabel keyframeRotationLabel = new JLabel("rotation: ");

    // creates the text fields.
    int keyframeColSize = 5;
    keyframeTimeText = new JTextField("", keyframeColSize);
    keyframeXText = new JTextField("", keyframeColSize);
    keyframeYText = new JTextField("", keyframeColSize);
    keyframeWText = new JTextField("", keyframeColSize);
    keyframeHText = new JTextField("", keyframeColSize);
    keyframeRText = new JTextField("", keyframeColSize);
    keyframeGText = new JTextField("", keyframeColSize);
    keyframeBText = new JTextField("", keyframeColSize);
    keyframeRotationText = new JTextField("", keyframeColSize);

    // adds the labels and text fields to the panel.
    keyframePanel.add(keyframeBlankTextLabel);
    keyframePanel.add(keyframeBlankTextLabel2);
    keyframePanel.add(keyframeTimeTextLabel);
    keyframePanel.add(keyframeTimeText);
    keyframePanel.add(keyframeXTextLabel);
    keyframePanel.add(keyframeXText);
    keyframePanel.add(keyframeYTextLabel);
    keyframePanel.add(keyframeYText);
    keyframePanel.add(keyframeWTextLabel);
    keyframePanel.add(keyframeWText);
    keyframePanel.add(keyframeHTextLabel);
    keyframePanel.add(keyframeHText);
    keyframePanel.add(keyframeRTextLabel);
    keyframePanel.add(keyframeRText);
    keyframePanel.add(keyframeGTextLabel);
    keyframePanel.add(keyframeGText);
    keyframePanel.add(keyframeBTextLabel);
    keyframePanel.add(keyframeBText);
    keyframePanel.add(keyframeRotationLabel);
    keyframePanel.add(keyframeRotationText);

    // deals with creating and maintaining the JLists of shapes and keyframes

    defaultLayerList = new DefaultListModel();
    layerList = new JList<>(defaultLayerList);
    for (int i = 0; i < timeline.size(); i++) {
      defaultLayerList.addElement(i + 1);
    }

    defaultShapeListModel = new DefaultListModel();
    shapeList = new JList<>(defaultShapeListModel);

    defaultKeyframeListModel = new DefaultListModel();
    keyframeList = new JList<Keyframe>(defaultKeyframeListModel);

    layerList.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        layerSelected = layerList.getSelectedIndex();
        defaultShapeListModel.clear();
        if (layerSelected != -1) {
          for (String namesOfShape :
                  getSelectedLayer(layerSelected).keySet()) {
            defaultShapeListModel.addElement(namesOfShape);
          }
        }
      }
    });


    shapeList.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          //This line prevents double events
          shapeSelected = shapeList.getSelectedValue();
          defaultKeyframeListModel.clear();
          if (shapeSelected != null) {
            for (int i = 0; i < getSelectedKeyframes(layerSelected, shapeSelected).size(); i++) {
              defaultKeyframeListModel.addElement(i + 1);
            }
          }
          keyframeTimeText.setText("");
          keyframeXText.setText("");
          keyframeYText.setText("");
          keyframeWText.setText("");
          keyframeHText.setText("");
          keyframeRText.setText("");
          keyframeGText.setText("");
          keyframeBText.setText("");
          keyframeRotationText.setText("");
        }
      }
    });

    keyframeList.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          //This line prevents double events
          keyframeSelectedNum = keyframeList.getSelectedIndex();
          if (keyframeSelectedNum != -1) {
            selected =
                    getSelectedKeyframes(layerSelected, shapeSelected).get(keyframeSelectedNum);

            // The timeline that this refers to is the old one. Where is this pointing and how
            // can we update this?
            // this happens when you add a new keyframe. the Jlist updates but when you click on
            // it, the timeline here does not have it so there is an indexoutofboundsexception


            int time = selected.getTime();
            int x = selected.getX();
            int y = selected.getY();
            int w = selected.getW();
            int h = selected.getH();
            int r = selected.getR();
            int g = selected.getG();
            int b = selected.getB();
            int rotation = selected.getRotation();
            keyframeTimeText.setText(Integer.toString(time));
            keyframeXText.setText(Integer.toString(x));
            keyframeYText.setText(Integer.toString(y));
            keyframeWText.setText(Integer.toString(w));
            keyframeHText.setText(Integer.toString(h));
            keyframeRText.setText(Integer.toString(r));
            keyframeGText.setText(Integer.toString(g));
            keyframeBText.setText(Integer.toString(b));
            keyframeRotationText.setText(Integer.toString(rotation));
          }
        }
      }
    });


    buttonPanel.add(startButton);
    buttonPanel.add(pauseButton);
    buttonPanel.add(restartButton);
    buttonPanel.add(loopEnableButton);
    buttonPanel.add(loopDisableButton);
    buttonPanel.add(increaseDelayButton);
    buttonPanel.add(normalDelayButton);
    buttonPanel.add(decreaseDelayButton);
    buttonPanel.add(addShapeButton);
    buttonPanel.add(addKeyframeButton);
    buttonPanel.add(saveButton);
    buttonPanel.add(loadButton);
    buttonPanel.add(addLayer);
    buttonPanel.add(swapLayer);
    buttonPanel.add(deleteLayer);

    listPanel.add(new JScrollPane(layerList));
    DefaultListCellRenderer renderer3 = (DefaultListCellRenderer) layerList.getCellRenderer();
    renderer3.setHorizontalAlignment(JLabel.CENTER);
    listPanel.add(new JScrollPane(shapeList));
    DefaultListCellRenderer renderer = (DefaultListCellRenderer) shapeList.getCellRenderer();
    renderer.setHorizontalAlignment(JLabel.CENTER);
    listPanel.add(new JScrollPane(keyframeList));
    DefaultListCellRenderer renderer2 = (DefaultListCellRenderer) keyframeList.getCellRenderer();
    renderer2.setHorizontalAlignment(JLabel.CENTER);

    overallPanel.add(buttonPanel);
    overallPanel.add(listPanel);
    overallPanel.add(keyframePanel);
    overallPanel.add(keyframeButtonPanel);


    // adds the scrubbing feature
    scrubb = new Scrollbar(Scrollbar.HORIZONTAL,0,10,0,
            visualPanel.lastAnimation());
    scrubb.addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent e) {
        if (scrubb.getValueIsAdjusting()) {
          visualPanel.setTimeInPanel(scrubb.getValue());
        }
      }
    });
    overallPanel.add(scrubb);

    this.add(overallPanel, BorderLayout.WEST);

    // Initializing the Timer.
    timer = new Timer(delay, visualPanel);
    timer.start();

    this.pack();
  }

  @Override
  public Appendable print() {
    throw new UnsupportedOperationException("Visual doesn't need a print method.");
  }

  @Override
  public void setListener(IActionListener listener) {
    ActionListener listen = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        listener.action(e.getActionCommand());
      }
    };
    startButton.addActionListener(listen);
    pauseButton.addActionListener(listen);
    restartButton.addActionListener(listen);
    loopEnableButton.addActionListener(listen);
    loopDisableButton.addActionListener(listen);
    increaseDelayButton.addActionListener(listen);
    normalDelayButton.addActionListener(listen);
    decreaseDelayButton.addActionListener(listen);
    addShapeButton.addActionListener(listen);
    addKeyframeButton.addActionListener(listen);
    deleteShapeButton.addActionListener(listen);
    setButton.addActionListener(listen);
    deleteKeyframeButton.addActionListener(listen);
    saveButton.addActionListener(listen);
    loadButton.addActionListener(listen);
    addLayer.addActionListener(listen);
    swapLayer.addActionListener(listen);
    deleteLayer.addActionListener(listen);
  }

  @Override
  public void changeDelay(int change) {
    if (delay + change >= 0) {
      this.delay = this.delay + change;
    }
    else {
      this.delay = 1;
    }
    timer.setDelay(delay);
  }

  @Override
  public int getLayerSelected() {
    return layerSelected;
  }

  @Override
  public String getShapeSelected() {
    return shapeSelected;
  }

  @Override
  public int getKeyframeSelectedNum() {
    return keyframeSelectedNum;
  }

  @Override
  public void clearDefaultLayerListModel() {
    defaultLayerList.clear();
  }

  @Override
  public void clearDefaultShapeListModel() {
    defaultShapeListModel.clear();
  }

  @Override
  public void addKeyframeToDefaultKeyframeListModel(int keyframeIndex) {
    defaultKeyframeListModel.addElement(keyframeIndex);
  }

  @Override
  public void clearDefaultKeyframeListModel() {
    defaultKeyframeListModel.clear();
  }

  @Override
  public void addIndexToDefaultLayerListModel(int index) {
    defaultLayerList.addElement(index + 1);
  }

  @Override
  public void addShapeToDefaultShapeListModel(String shapeName) {
    defaultShapeListModel.addElement(shapeName);
  }

  @Override
  public String getKeyframeTimeText() {
    return keyframeTimeText.getText();
  }

  @Override
  public String getKeyframeXText() {
    return keyframeXText.getText();
  }

  @Override
  public String getKeyframeYText() {
    return keyframeYText.getText();
  }

  @Override
  public String getKeyframeWText() {
    return keyframeWText.getText();
  }

  @Override
  public String getKeyframeHText() {
    return keyframeHText.getText();
  }

  @Override
  public String getKeyframeRText() {
    return keyframeRText.getText();
  }

  @Override
  public String getKeyframeGText() {
    return keyframeGText.getText();
  }

  @Override
  public String getKeyframeBText() {
    return keyframeBText.getText();
  }

  @Override
  public String getKeyframeRotationText() {
    return keyframeRotationText.getText();
  }

  @Override
  public void startTimer() {
    timer.start();
  }

  @Override
  public void stopTimer() {
    timer.stop();
  }

  @Override
  public void resetDelay() {
    timer.setDelay(timer.getInitialDelay());
  }

  @Override
  public void restartTimer() {
    visualPanel.setTimeInPanel(0);
  }

  @Override
  public void enableLooping(boolean looping) {
    visualPanel.setLooping(looping);
  }

  @Override
  public void updateTimeline(ArrayList<LinkedHashMap<String, IShape>> timeline) {
    this.timeline = timeline;
    this.visualPanel.setTimeline(timeline);
  }

  /**
   * Gets the keyframes of the specified shape.
   *
   * @param shapeName name of shape
   * @return list of shape's keyframes
   */
  private ArrayList<Keyframe> getSelectedKeyframes(int layer, String shapeName) {
    return timeline.get(layer).get(shapeName).getKeyframes();
  }

  public void setVisibility(boolean visibility) {
    super.setVisible(visibility);
  }

  private LinkedHashMap<String, IShape> getSelectedLayer(int index) {
    return this.timeline.get(index);
  }
}