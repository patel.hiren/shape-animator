package cs3500.animator.view;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.JPanel;

import cs3500.animator.model.IShape;
import cs3500.animator.model.Keyframe;
import cs3500.animator.model.Transformation;

/** The panel that displays the actual animation. */
public class VisualPanel extends JPanel implements ActionListener {

  private ArrayList<LinkedHashMap<String, IShape>> timeline;
  private int minX;
  private int minY;
  private int sceneWidth;
  private int sceneHeight;
  private int time;
  private boolean looping;

  /**
   * Constructor.
   * @param timeline the timeline
   * @param minX the minx
   * @param minY the miny
   * @param sceneWidth the width
   * @param sceneHeight height
   */
  public VisualPanel(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX, int minY,
                     int sceneWidth,
                     int sceneHeight) {
    this.setBackground(Color.WHITE);
    this.timeline = timeline;
    this.minX = minX;
    this.minY = minY;
    this.sceneWidth = sceneWidth;
    this.sceneHeight = sceneHeight;
    this.time = 0;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    repaint();
    if (looping) {
      timeLoop();
    }
    time++;
  }

  @Override
  public Dimension getPreferredSize() {
    return new Dimension(sceneWidth,sceneHeight);
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    Graphics2D g2d = (Graphics2D) g;

    g2d.translate(-minX, -minY);

    for (int k = 0; k < timeline.size(); k++) {
      for (IShape s : timeline.get(k).values()) {
        String shapeType = s.figureOutShape();
        for (int i = 0; i < s.getKeyframes().size() - 1; i++) {
          Keyframe start = s.getKeyframes().get(i);
          Keyframe end = s.getKeyframes().get(i + 1);

          AffineTransform transform = new AffineTransform();

          AffineTransform old = g2d.getTransform();

          if (start.getTime() <= time && time <= end.getTime()) {
            g2d.setColor(new Color(
                    this.tweening(start.getR(), end.getR(), start.getTime(), end.getTime()),
                    this.tweening(start.getG(), end.getG(), start.getTime(), end.getTime()),
                    this.tweening(start.getB(), end.getB(), start.getTime(), end.getTime())));

            int x = this.tweening(start.getX(), end.getX(), start.getTime(), end.getTime());
            int y = this.tweening(start.getY(), end.getY(), start.getTime(), end.getTime());
            int w = this.tweening(start.getW(), end.getW(), start.getTime(), end.getTime());
            int h = this.tweening(start.getH(), end.getH(), start.getTime(), end.getTime());

            transform.rotate(Math.toRadians(this.tweening(start.getRotation(), end.getRotation(),
                    start.getTime(), end.getTime())), x + w / 2, y + h / 2);

            g2d.transform(transform);

            if (shapeType.equalsIgnoreCase("rectangle")) {
              g2d.fillRect(x, y, w, h);
            } else if (shapeType.equalsIgnoreCase("oval")) {
              g2d.fillOval(x, y, w, h);
            }

            g2d.setTransform(old);

          }

        }
      }
    }
  }

  /**
   * Performs the tweening operation of determining the current value of an copy attribute given
   * its starting value, its ending value, and times.
   *
   * @param start starting attribute value
   * @param end ending attribute value
   * @param startTime starting attribute time
   * @param endTime ending attribute time
   * @return current attribute value
   */
  private int tweening(int start, int end, int startTime, int endTime) {
    return (int)(start * ((double)(endTime - time) / (double)(endTime - startTime))
            + end * ((double)(time - startTime) / (double)(endTime - startTime)));
  }

  /**
   * Sets time.
   *
   * @param time time
   */
  public void setTimeInPanel(int time) {
    this.time = time;
  }

  /** Performs looping functionality by resetting time whenever the animation ends. */
  public void timeLoop() {
    int maxTime = lastAnimation();
    if (time >= maxTime) {
      time = 0;
    }
  }

  /**
   * Sets looping.
   *
   * @param looping looping
   */
  public void setLooping(boolean looping) {
    this.looping = looping;
  }

  /**
   * Finds the time of the last keyframe in this animation.
   *
   * @return max time
   */
  public int lastAnimation() {
    int maxTime = 0;

    for (int i = 0; i < timeline.size(); i++) {
      for (IShape s : timeline.get(i).values()) {
        for (Transformation t : s.getTransformations()) {
          if (t.getEndTime() > maxTime) {
            maxTime = t.getEndTime();
          }
        }
      }
    }

    return maxTime;
  }

  /**
   * Sets timeline.
   *
   * @param timeline timeline
   */
  public void setTimeline(ArrayList<LinkedHashMap<String, IShape>> timeline) {
    this.timeline = timeline;
  }

  /**
   * Gets the current time of the animations.
   * @return the current time
   */
  public int getCurrentTime() {
    return this.time;
  }

}