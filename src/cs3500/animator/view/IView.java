package cs3500.animator.view;

public interface IView {
  /**
   * In TextView and SVGAnimationView, returns the full script for the animation.
   *
   * @return the animation script
   * @throws UnsupportedOperationException if Composite or VisualAnimation
   */
  Appendable print();
}