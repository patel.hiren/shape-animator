package cs3500.animator.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import cs3500.animator.model.IShape;

/** Abstraction of TextView and SVGAnimationView. */
public class AbstractView implements IView {

  protected ArrayList<LinkedHashMap<String, IShape>> timeline;
  protected int minX;
  protected int minY;
  protected int sceneWidth;
  protected int sceneHeight;

  /**
   * Makes the constructor.
   * @param timeline the timeline
   * @param minX the minx
   * @param minY the miny
   * @param sceneWidth the width
   * @param sceneHeight the height
   */
  public AbstractView(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX,
                      int minY, int sceneWidth, int sceneHeight) {
    this.timeline = timeline;
    this.minX = minX;
    this.minY = minY;
    this.sceneWidth = sceneWidth;
    this.sceneHeight = sceneHeight;
  }


  /**
   * Prints the appendable.
   *
   * @return appendable
   */
  @Override
  public Appendable print() {
    return null;
  }
}