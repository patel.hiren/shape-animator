package cs3500.animator.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import cs3500.animator.controller.IActionListener;
import cs3500.animator.model.IShape;

public interface IEditView extends IView {

  /**
   * Gets the name of the layers currently selected in the JList of layer names.
   *
   * @return layer selected
   */
  int getLayerSelected();

  /**
   * Gets the name of the shape currently selected in the JList of shape names.
   *
   * @return shape selected
   */
  String getShapeSelected();

  /**
   * Gets the index of the keyframe currently selected in the JList of keyframe indices.
   *
   * @return the index
   */
  int getKeyframeSelectedNum();

  /** Clears the JList of shapes. */
  void clearDefaultShapeListModel();

  /** Clears the JList of keyframes. */
  void clearDefaultKeyframeListModel();

  /**
   * Adds a keyframe index to the JList of keyframe indices.
   *
   * @param keyframeIndex added keyframe index
   */
  void addKeyframeToDefaultKeyframeListModel(int keyframeIndex);

  /**
   * Adds a shape name to the JList of shape names.
   *
   * @param shapeName added shape name
   */
  void addShapeToDefaultShapeListModel(String shapeName);

  /**
   * Creates an ActionListener with the given IActionListener
   * and assigns it as every button's ActionListener.
   *
   * @param listener given IActionListener
   */
  void setListener(IActionListener listener);

  /**
   * Gets the text currently in the time text field.
   *
   * @return time
   */
  String getKeyframeTimeText();

  /**
   * Gets the text currently in the x text field.
   *
   * @return x
   */
  String getKeyframeXText();

  /**
   * Gets the text currently in the y text field.
   *
   * @return y
   */
  String getKeyframeYText();

  /**
   * Gets the text currently in the w text field.
   *
   * @return w
   */
  String getKeyframeWText();

  /**
   * Gets the text currently in the h text field.
   *
   * @return h
   */
  String getKeyframeHText();

  /**
   * Gets the text currently in the r text field.
   *
   * @return r
   */
  String getKeyframeRText();

  /**
   * Gets the text currently in the g text field.
   *
   * @return g
   */
  String getKeyframeGText();

  /**
   * Gets the text currently in the b text field.
   *
   * @return b
   */
  String getKeyframeBText();

  /**
   * Gets the text currently in the rotation text field.
   *
   * @return rotation
   */
  String getKeyframeRotationText();

  /** Starts the timer. */
  void startTimer();

  /** Stops the timer. */
  void stopTimer();

  /**
   * Changes the timer delay by the specified amount.
   *
   * @param change change to timer delay
   */
  void changeDelay(int change);

  /** Returns the timer delay to its initial value. */
  void resetDelay();

  /** Sets time back to 0. */
  void restartTimer();

  /**
   * Sets the animation's looping functionality.
   *
   * @param looping whether or not the animation will loop
   */
  void enableLooping(boolean looping);

  /**
   * Sets the timeline of shapes in both this frame and its animation panel.
   *
   * @param timeline the updated timeline
   */
  void updateTimeline(ArrayList<LinkedHashMap<String, IShape>> timeline);

  /**
   * Sets the visibility of the frame.
   * @param visibility the visibility of the frame.
   */
  void setVisibility(boolean visibility);

  /**
   * Clears the layer JList.
   */
  void clearDefaultLayerListModel();

  /**
   * adds the index back to the Layer list.
   * @param index layerlist index
   */
  void addIndexToDefaultLayerListModel(int index);
}