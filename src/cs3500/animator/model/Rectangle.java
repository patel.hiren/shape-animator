package cs3500.animator.model;

import java.util.ArrayList;

/** Represents a rectangle shape. */
public class Rectangle extends AbstractShape {

  public Rectangle() {
    super();
  }

  public Rectangle(ArrayList<Transformation> transformations, ArrayList<Keyframe> keyframes) {
    super(transformations, keyframes);
  }

  @Override
  public AbstractShape copy() {
    AbstractShape s = new Rectangle(getTransformations(), getKeyframes());

    return s;
  }

  @Override
  public String figureOutShape() {
    return "rectangle";
  }

  @Override
  public String svgText(String name, int minX, int minY) {
    Transformation first = this.getTransformations().get(0);
    String quote = "\"";

    StringBuilder sb = new StringBuilder();
    sb.append("<rect id=" + quote + name + quote +
            " x=" + quote + (first.getStartX() - minX) + quote +
            " y=" + quote + (first.getStartY() - minY) + quote +
            " width=" + quote + first.getStartWidth() + quote +
            " height=" + quote + first.getStartHeight() + quote +
            " fill=" + quote +
            "rgb(" +
            first.getStartR() + "," +
            first.getStartG() + "," +
            first.getStartB() +
            ")" + quote +
            " opacity=\"0\" >\n");

    sb.append(beg(first.getStartTime() * 100, 0));
    sb.append(attr("opacity", 0, 1));

    sb.append(beg(first.getStartTime() * 100, (200 - first.getStartTime()) * 100));
    sb.append(attr("opacity", 1, 1));

    for (Transformation t : this.getTransformations()) {
      String beginning = beg(t.getStartTime() * 100, (t.getEndTime() - t.getStartTime()) * 100);

      sb.append(beginning + attr("x", t.getStartX() - minX, t.getEndX() - minX));
      sb.append(beginning + attr("y", t.getStartY() - minY, t.getEndY() - minY));
      sb.append(beginning + attr("width", t.getStartWidth(), t.getEndWidth()));
      sb.append(beginning + attr("height", t.getStartHeight(), t.getEndHeight()));
      sb.append(beginning + fill(t));
    }

    sb.append("</rect>\n");

    return sb.toString();
  }
}