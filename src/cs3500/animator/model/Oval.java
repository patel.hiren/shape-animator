package cs3500.animator.model;

import java.util.ArrayList;

/** Represents an oval shape. */
public class Oval extends AbstractShape {

  public Oval() {
    super();
  }

  public Oval(ArrayList<Transformation> transformations, ArrayList<Keyframe> keyframes) {
    super(transformations, keyframes);
  }

  @Override
  public AbstractShape copy() {
    return new Oval(getTransformations(), getKeyframes());
  }

  @Override
  public String figureOutShape() {
    return "oval";
  }

  @Override
  public String svgText(String name, int minX, int minY) {
    Transformation first = this.getTransformations().get(0);
    String quote = "\"";
    StringBuilder sb = new StringBuilder();
    sb.append("<ellipse id=" + quote + name + quote +
            " cx=" + quote + (first.getStartX() - minX) + quote +
            " cy=" + quote + (first.getStartY() - minY) + quote +
            " rx=" + quote + (first.getStartWidth() / 2) + quote +
            " ry=" + quote + (first.getStartHeight() / 2) + quote +
            " fill=" + quote + "rgb(" + first.getStartR() + "," +
            first.getStartG() + "," +
            first.getStartB() + ")" + quote +
            " opacity=\"0\" >\n");

    sb.append(beg(first.getStartTime() * 100, 0));
    sb.append(attr("opacity", 0, 1));

    sb.append(beg(first.getStartTime() * 100, (200 - first.getStartTime()) * 100));
    sb.append(attr("opacity", 1, 1));

    for (Transformation t : this.getTransformations()) {
      String beginning = beg(t.getStartTime() * 100, (t.getEndTime() - t.getStartTime()) * 100);

      sb.append(beginning + attr("cx", t.getStartX() - minX, t.getEndX() - minX));
      sb.append(beginning + attr("cy", t.getStartY() - minY, t.getEndY() - minY));
      sb.append(beginning + attr("rx", t.getStartWidth() / 2, t.getEndWidth() / 2));
      sb.append(beginning + attr("ry", t.getStartHeight() / 2, t.getEndHeight() / 2));
      sb.append(beginning + fill(t));
    }
    sb.append("</ellipse>\n");

    return sb.toString();
  }
}