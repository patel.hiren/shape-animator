package cs3500.animator.model;

import java.util.ArrayList;

/** Interface allows many shapes to implement it. */
public interface IShape {
  /**
   * Describes what kind of shape this is.
   *
   * @return a string representation of the shape type.
   */
  String figureOutShape();

  /**
   * Outputs the svg format.
   *
   * @param name    the name of the shape
   * @param minX    the minimum X-value
   * @param minY    the minimum Y-value
   */
  String svgText(String name, int minX, int minY);

  /**
   * Adds a Transformation to the list of Transformations.
   *
   * @param t the transformation being added
   */
  void addTransformation(Transformation t);

  /**
   * Makes a copy of the shape, to protect this shape from unwanted mutations.
   *
   * @return a copy of the shape
   */
  IShape copy();

  /**
   * Gets the list of transformations.
   *
   * @return transformation list
   */
  ArrayList<Transformation> getTransformations();

  /**
   * Gets the list of keyframes.
   *
   * @return keyframe list.
   */
  ArrayList<Keyframe> getKeyframes();

  /**
   * Describes the change of a specific attribute, for SVG.
   *
   * @param name name of attribute
   * @param from "from" value
   * @param to "to" value
   * @return svg-formatted script
   */
  String attr(String name, int from, int to);

  /**
   * Creates the block of SVG script that appears before every described motion.
   *
   * @param begin beginning time of motion
   * @param dur duration of motion
   * @return svg-formatted script
   */
  String beg(int begin, int dur);

  /**
   * Describes the color change of a given Transformation, in SVG format.
   *
   * @param t Transformation
   * @return svg-formatted script
   */
  String fill(Transformation t);

  /**
   * Removes a specific keyframe from the list of keyframes.
   *
   * @param index the index of the keyframe to be removed
   */
  void removeKeyframe(int index);

  /**
   * Adds a keyframe to the list of keyframes
   * in the appropriate position, determined by the given time.
   *
   * @param time the time of the keyframe to be added
   */
  void addKeyframe(int time);

  /**
   * Updates a specific keyframe in the list of keyframes with a new set of attributes.
   *
   * @param keyframeIndex the index of the keyframe being updated
   * @param time the updated time value
   * @param x the updated x value
   * @param y the updated y value
   * @param w the updated width value
   * @param h the updated height value
   * @param r the updated red value
   * @param g the updated green value
   * @param b the updated blue value
   */
  void updateKeyframe(int keyframeIndex, int time, int x, int y, int w, int h, int r, int g,
                      int b, int rotation);

  /**
   * Adds a predefined keyframe to the list of keyframes.
   *
   * @param kf keyframe being added
   */
  void addKeyframeFromBuilder(Keyframe kf);
}