package cs3500.animator.model;

import java.util.ArrayList;


/**
 * A shape contains an ArrayList of its transformations and a method that returns its shape type as
 * a string.
 */
public abstract class AbstractShape implements IShape {

  private ArrayList<Transformation> transformations;
  private ArrayList<Keyframe> keyframes;

  /** Default constructor initializes both lists as empty. */
  public AbstractShape() {
    this.transformations = new ArrayList<>();
    this.keyframes = new ArrayList<>();
  }

  /**
   * Constructor assigns given ArrayLists to transformations and keyframes, respectively.
   *
   * @param transformations list of this shape's transformations
   * @param keyframes list of this shape's keyframes
   */
  public AbstractShape(ArrayList<Transformation> transformations, ArrayList<Keyframe> keyframes) {
    this.transformations = transformations;
    this.keyframes = keyframes;
  }

  @Override
  public String figureOutShape() {
    return null;
  }

  @Override
  public ArrayList<Transformation> getTransformations() {
    return (ArrayList) transformations.clone();
  }

  @Override
  public ArrayList<Keyframe> getKeyframes() {
    return (ArrayList) keyframes.clone();
  }

  @Override
  public void addTransformation(Transformation t) {
    transformations.add(t);
  }

  @Override
  public void addKeyframeFromBuilder(Keyframe kf) {
    keyframes.add(kf);
  }

  @Override
  public String attr(String name, int from, int to) {
    String quote = "\"";
    return (quote + name + quote
            + " from=" + quote + from + quote
            + " to=" + quote + to + quote
            + " fill=" + quote + "freeze" + quote + " />\n");
  }

  @Override
  public String beg(int begin, int dur) {
    String quote = "\"";
    return ("<animate attributeType=\"xml\"" +
            " begin=" + quote + begin + "ms" + quote +
            " dur=" + quote + dur + "ms" + quote +
            " attributeName=");
  }

  @Override
  public String fill(Transformation t) {
    String quote = "\"";
    return ("\"fill\"" +
            " from=" + quote + "rgb(" + t.getStartR() + "," +
            t.getStartG() + "," +
            t.getStartB() + ")" + quote +
            " to=" + quote + "rgb(" + t.getEndR() + "," +
            t.getEndG() + "," +
            t.getEndB() + ")" + quote +
            " fill=\"freeze\" />\n");
  }

  @Override
  public void removeKeyframe(int index) {
    keyframes.remove(index);
  }

  @Override
  public void addKeyframe(int time) {
    if (keyframes.size() == 0) {
      keyframes.add(new Keyframe(time, 0, 0, 0, 0, 0, 0, 0));
    } else if (keyframes.get(0).getTime() > time) {
      Keyframe first = keyframes.get(0);
      keyframes.add(0, new Keyframe(time, first.getX(), first.getY(), first.getW(), first.getH(),
              first.getR(), first.getG(), first.getB()));
    } else if (keyframes.get(keyframes.size() - 1).getTime() < time) {
      Keyframe last = keyframes.get(keyframes.size() - 1);
      keyframes.add(new Keyframe(time, last.getX(), last.getY(), last.getW(), last.getH(),
              last.getR(), last.getG(), last.getB()));
    } else {
      for (int i = 0; i < keyframes.size() - 1; i++) {
        if (keyframes.get(i + 1).getTime() > time) {
          Keyframe start = keyframes.get(i);
          Keyframe end = keyframes.get(i + 1);
          int x = this.tweening(start.getX(), end.getX(), start.getTime(), end.getTime(), time);
          int y = this.tweening(start.getY(), end.getY(), start.getTime(), end.getTime(), time);
          int w = this.tweening(start.getW(), end.getW(), start.getTime(), end.getTime(), time);
          int h = this.tweening(start.getH(), end.getH(), start.getTime(), end.getTime(), time);
          int r = this.tweening(start.getR(), end.getR(), start.getTime(), end.getTime(), time);
          int g = this.tweening(start.getG(), end.getG(), start.getTime(), end.getTime(), time);
          int b = this.tweening(start.getB(), end.getB(), start.getTime(), end.getTime(), time);
          keyframes.add(i + 1, new Keyframe(time, x, y, w, h, r, g, b));
          break;
        }
      }
    }
  }

  /**
   * Performs the tweening operation of determining the current value of an copy attribute given
   * its starting value, its ending value, and times.
   *
   * @param start starting attribute value
   * @param end ending attribute value
   * @param startTime starting attribute time
   * @param endTime ending attribute time
   * @param time current time
   * @return current attribute value
   */
  private int tweening(int start, int end, int startTime, int endTime, int time) {
    return (int) Math.round(start * ((double) (endTime - time) / (double) (endTime - startTime))
            + end * ((double) (time - startTime) / (double) (endTime - startTime)));
  }

  @Override
  public void updateKeyframe(int keyframeIndex,
                             int time, int x, int y, int w, int h, int r, int g, int b,
                             int rotation) {
    Keyframe kf = keyframes.get(keyframeIndex);
    kf.setTime(time);
    kf.setX(x);
    kf.setY(y);
    kf.setW(w);
    kf.setH(h);
    kf.setR(r);
    kf.setG(g);
    kf.setB(b);
    kf.setRotation(rotation);
  }
}