package cs3500.animator.model;

import sun.awt.image.ImageWatched;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import cs3500.animator.util.AnimationBuilder;

/** An entire animation, with a LinkedHashMap of featured shapes. */
public class Animation implements IModel {
  private int minX;
  private int minY;
  private int sceneWidth;
  private int sceneHeight;
  /**
   * Represents all shapes featured in this animation,
   * each with a field containing their own history of transformations.
   */
  private ArrayList<LinkedHashMap<String, IShape>> timeline;


  /**
   * Custom Animation constructor.
   * @param timeline hashmap of all shapes featured in this animation
   * @param minX the minimum x-value
   * @param minY the minimum y-value
   * @param sceneWidth the scene width
   * @param sceneHeight the scene height
   */
  public Animation(ArrayList<LinkedHashMap<String, IShape>> timeline, int minX, int minY,
                   int sceneWidth,
                   int sceneHeight) {
    this.timeline = timeline;
    this.minX = minX;
    this.minY = minY;
    this.sceneWidth = sceneWidth;
    this.sceneHeight = sceneHeight;
  }

  @Override
  public ArrayList<LinkedHashMap<String, IShape>> getTimeline() {
    ArrayList<LinkedHashMap<String, IShape>> returnableTimeline = new ArrayList<>();

    for (int i = 0; i < timeline.size(); i++) {
      if (returnableTimeline.size() - 1 < i) {
        returnableTimeline.add(i, new LinkedHashMap<>());
      }
      for (Map.Entry<String, IShape> entry : timeline.get(i).entrySet()) {
        String key = entry.getKey();
        IShape s = entry.getValue();
         returnableTimeline.get(i).put(key, s.copy());
      }
    }
    return returnableTimeline;
  }

  @Override
  public void addShape(int layer, String shape, String name) {
    if (shape.equalsIgnoreCase("rectangle")) {
      timeline.get(layer).put(name, new Rectangle());
    } else if (shape.equalsIgnoreCase("oval")) {
      timeline.get(layer).put(name, new Oval());
    } else {
      throw new IllegalArgumentException("Please enter a valid shape");
    }
  }

  @Override
  public int getMinX() {
    return minX;
  }

  @Override
  public int getMinY() {
    return minY;
  }

  @Override
  public int getSceneWidth() {
    return sceneWidth;
  }

  @Override
  public int getSceneHeight() {
    return sceneHeight;
  }

  @Override
  public void addTransformationToShape(int layer, String name, Transformation t) {
    timeline.get(layer).get(name).addTransformation(t);
  }

  @Override
  public void addKeyframeToShape(int layer, String name, int time) {
    timeline.get(layer).get(name).addKeyframe(time);
  }

  @Override
  public void removeShape(int layer, String name) {
    timeline.get(layer).remove(name);
  }

  @Override
  public void updateKeyframeInShape(int layer, String shapeName, int keyframeIndex,
                                    int time, int x, int y, int w, int h, int r, int g, int b,
                                    int rotation) {
    timeline.get(layer).get(shapeName).updateKeyframe(keyframeIndex, time, x, y, w, h, r, g, b, rotation);
  }

  @Override
  public void removeKeyframeFromShape(int layer, String shapeName, int keyframeIndex) {
    timeline.get(layer).get(shapeName).removeKeyframe(keyframeIndex);
  }

  @Override
  public void addLayer() {
    timeline.add(new LinkedHashMap<String, IShape>());
  }

  @Override
  public void swapLayers(int layerOne, int layerTwo) {
    LinkedHashMap<String, IShape> temp = timeline.get(layerOne);
    timeline.set(layerOne, timeline.get(layerTwo));
    timeline.set(layerTwo, temp);
  }

  @Override
  public void deleteLayer(int layer) {
    timeline.remove(layer);
  }

  public static final class Builder implements AnimationBuilder<IModel> {

    private ArrayList<LinkedHashMap<String, IShape>> nestedTimeline =  new ArrayList<>();

    private int minX = 0;
    private int minY = 0;
    private int sceneWidth = 500;
    private int sceneHeight = 500;


    @Override
    public IModel build() {
      return new Animation(nestedTimeline, minX, minY, sceneWidth,
              sceneHeight);
    }

    @Override
    public AnimationBuilder<IModel> setBounds(int x, int y, int width, int height) {
      this.minX = x;
      this.minY = y;
      this.sceneWidth = width;
      this.sceneHeight = height;

      return this;
    }

    @Override
    public AnimationBuilder<IModel> declareShape(int layer, String name, String type) {

      if (type.equalsIgnoreCase("rectangle")) {
        if (nestedTimeline.size() - 1 < layer) {
          nestedTimeline.add(layer, new LinkedHashMap<>());
          nestedTimeline.get(layer).put(name, new Rectangle());
        }
        else {
          nestedTimeline.get(layer).put(name, new Rectangle());
        }
      } else if (type.equalsIgnoreCase("oval")
              || type.equalsIgnoreCase("ellipse")) {
        if (nestedTimeline.size() - 1 < layer) {
          nestedTimeline.add(layer, new LinkedHashMap<>());
          nestedTimeline.get(layer).put(name, new Oval());
        }
        else {
          nestedTimeline.get(layer).put(name, new Oval());
        }
      } else {
        throw new IllegalArgumentException("Please enter a valid shape.");
      }

      return this;
    }

    @Override
    public AnimationBuilder<IModel> addMotion(String name, int t1, int x1, int y1,
                                              int w1,
                                              int h1, int r1, int g1, int b1, int t2, int x2,
                                              int y2, int w2, int h2, int r2, int g2, int b2) {

      Transformation t = new Transformation(t1, t2, x1, x2, y1, y2, w1, w2, h1, h2, r1, r2, g1,
              g2, b1, b2);

      for (LinkedHashMap<String, IShape> hm : nestedTimeline) {
        for (Map.Entry<String, IShape> entry : hm.entrySet()) {
          String key = entry.getKey();

          if (key.equalsIgnoreCase(name)) {
            hm.get(name).addTransformation(t);
            addKeyframe(name, t2, x2, y2, w2, h2, r2, g2, b2);
          }
        }
      }
      return this;
    }

    @Override
    public AnimationBuilder<IModel> addKeyframe(String name, int t, int x, int y, int w,
                                                int h, int r, int g, int b) {

      Keyframe kf = new Keyframe(t, x, y, w, h, r, g, b);

      for (LinkedHashMap<String, IShape> hm : nestedTimeline) {
        for (Map.Entry<String, IShape> entry : hm.entrySet()) {
          String key = entry.getKey();

          if (key.equalsIgnoreCase(name)) {
            hm.get(name).addKeyframeFromBuilder(kf);
          }
        }
      }

      return this;
    }
  }
}