package cs3500.animator.model;

public interface IKeyframe {

  /**
   * Gets the appropriate field.
   *
   * @return starting time of the transformation (in ticks)
   */
  int getTime();

  /**
   * Gets the appropriate field.
   *
   * @return starting x-position of the shape being transformed
   */
  int getX();

  /**
   * Gets the appropriate field.
   *
   * @return starting y-position of the shape being transformed
   */
  int getY();

  /**
   * Gets the appropriate field.
   *
   * @return starting width of the shape being transformed
   */
  int getW();

  /**
   * Gets the appropriate field.
   *
   * @return starting height of the shape being transformed
   */
  int getH();

  /**
   * Gets the appropriate field.
   *
   * @return starting color value R of the shape being transformed
   */
  int getR();

  /**
   * Gets the appropriate field.
   *
   * @return starting color value G of the shape being transformed
   */
  int getG();

  /**
   * Gets the appropriate field.
   *
   * @return starting color value B of the shape being transformed
   */
  int getB();

  /**
   * Gets the appropriate field.
   *
   * @return the rotation amount
   */
  int getRotation();

  /**
   * Sets time.
   *
   * @param time time
   */
  void setTime(int time);

  /**
   * Sets x.
   *
   * @param x x
   */
  void setX(int x);

  /**
   * Sets y.
   *
   * @param y y
   */
  void setY(int y);

  /**
   * Sets width.
   *
   * @param w width
   */
  void setW(int w);

  /**
   * Sets height.
   *
   * @param h height
   */
  void setH(int h);

  /**
   * Sets red.
   *
   * @param r red
   */
  void setR(int r);

  /**
   * Sets green.
   *
   * @param g green
   */
  void setG(int g);

  /**
   * Sets blue.
   *
   * @param b blue
   */
  void setB(int b);

  /**
   * Sets rotation.
   *
   * @param rotation rotation
   */
  void setRotation(int rotation);

}
