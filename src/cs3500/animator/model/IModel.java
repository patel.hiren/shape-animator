package cs3500.animator.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;


public interface IModel {
  /**
   * Gets the timeline of shapes.
   * @return a deep copy of the timeline
   */
  ArrayList<LinkedHashMap<String, IShape>> getTimeline();

  /**
   * Adds the shape to the hashmap of shapes.
   * @param shape type of shape
   * @param name name of shape
   */
  void addShape(int layer, String shape, String name);

  /**
   * Removes the specified shape from the hashmap of shapes.
   *
   * @param name the name of the shape being removed
   */
  void removeShape(int layer, String name);

  /**
   * Adds a keyframe to a specific shape at a specific time.
   * @param name name of shape
   * @param time the time of the keyframe being added
   */
  void addKeyframeToShape(int layer, String name, int time);

  /**
   * Gets the minX.
   * @return minX
   */
  int getMinX();

  /**
   * Gets the minY.
   * @return minY
   */
  int getMinY();

  /**
   * Gets the scene width.
   * @return the scene width
   */
  int getSceneWidth();

  /**
   * Gets the scene height.
   * @return the scene height
   */
  int getSceneHeight();

  /**
   * Updates the values of the specified keyframe in the specified shape.
   *
   * @param shapeName the shape name
   * @param keyframeIndex the index of the keyframe
   * @param time updated time value
   * @param x updated x value
   * @param y updated y value
   * @param w updated width value
   * @param h updated height value
   * @param r updated red value
   * @param g updated green value
   * @param b updated blue value
   */
  void updateKeyframeInShape(int layer, String shapeName, int keyframeIndex,
                             int time, int x, int y, int w, int h, int r, int g, int b,
                             int rotation);

  /**
   * Removes the specified keyframe from the specified shape.
   *
   * @param shapeName the shape name
   * @param keyframeIndex the index of the keyframe
   */
  void removeKeyframeFromShape(int layer, String shapeName, int keyframeIndex);

  /**
   * Adds the given transformation to the specified shape.
   *
   * @param name name of shape
   * @param t transformation being added
   */
  void addTransformationToShape(int layer, String name, Transformation t);

  /**
   * adds a layer.
   */
  void addLayer();

  /**
   * swaps two layers
   * @param layerOne layer one
   * @param layerTwo layer two
   */
  void swapLayers(int layerOne, int layerTwo);

  /**
   * deletes a layer.
   * @param layer index of later to delete
   */
  void deleteLayer(int layer);
}