package cs3500.animator.model;

public interface ITransformation {

  /**
   * Gets the appropriate field.
   *
   * @return starting time of the transformation (in ticks)
   */
  int getStartTime();

  /**
   * Gets the appropriate field.
   *
   * @return ending time of the transformation (in ticks)
   */
  int getEndTime();

  /**
   * Gets the appropriate field.
   *
   * @return starting x-position of the shape being transformed
   */
  int getStartX();

  /**
   * Gets the appropriate field.
   *
   * @return ending x-position of the shape being transformed
   */
  int getEndX();

  /**
   * Gets the appropriate field.
   *
   * @return starting y-position of the shape being transformed
   */
  int getStartY();

  /**
   * Gets the appropriate field.
   *
   * @return ending y-position of the shape being transformed
   */
  int getEndY();

  /**
   * Gets the appropriate field.
   *
   * @return starting width of the shape being transformed
   */
  int getStartWidth();

  /**
   * Gets the appropriate field.
   *
   * @return ending width of the shape being transformed
   */
  int getEndWidth();

  /**
   * Gets the appropriate field.
   *
   * @return starting height of the shape being transformed
   */
  int getStartHeight();

  /**
   * Gets the appropriate field.
   *
   * @return ending height of the shape being transformed
   */
  int getEndHeight();

  /**
   * Gets the appropriate field.
   *
   * @return starting color value R of the shape being transformed
   */
  int getStartR();

  /**
   * Gets the appropriate field.
   *
   * @return ending color value R of the shape being transformed
   */
  int getEndR();

  /**
   * Gets the appropriate field.
   *
   * @return starting color value G of the shape being transformed
   */
  int getStartG();

  /**
   * Gets the appropriate field.
   *
   * @return ending color value G of the shape being transformed
   */
  int getEndG();

  /**
   * Gets the appropriate field.
   *
   * @return starting color value B of the shape being transformed
   */
  int getStartB();

  /**
   * Gets the appropriate field.
   *
   * @return ending color value B of the shape being transformed
   */
  int getEndB();

  /**
   * Gets the starting keyframe of this transformation.
   *
   * @return start keyframe
   */
  Keyframe getStartFrame();

  /**
   * Gets the ending keyframe of this transformation.
   *
   * @return end keyframe
   */
  Keyframe getEndFrame();

}
