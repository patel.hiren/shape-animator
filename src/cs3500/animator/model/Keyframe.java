package cs3500.animator.model;

/**
 * Represents the state of a shape at a specific time.
 * A collection of Keyframes is used to describe the animation of a shape.
 * */
public class Keyframe implements IKeyframe {

  private int time;
  private int x;
  private int y;
  private int w;
  private int h;
  private int r;
  private int g;
  private int b;
  private int rotation;

  /**
   * Constructor declaring all fields as specified.
   *
   * @param time the time at which this keyframe appears in the animation
   * @param x x-value
   * @param y y-value
   * @param w width value
   * @param h height value
   * @param r red value
   * @param g green value
   * @param b blue value
   */
  public Keyframe(int time, int x, int y, int w, int h, int r, int g, int b) {
    this.time = time;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.r = r;
    this.g = g;
    this.b = b;
    this.rotation = 0;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting time of the transformation (in ticks)
   */
  public int getTime() {
    return time;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting x-position of the shape being transformed
   */
  public int getX() {
    return x;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting y-position of the shape being transformed
   */
  public int getY() {
    return y;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting width of the shape being transformed
   */
  public int getW() {
    return w;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting height of the shape being transformed
   */
  public int getH() {
    return h;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting color value R of the shape being transformed
   */
  public int getR() {
    return r;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting color value G of the shape being transformed
   */
  public int getG() {
    return g;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting color value B of the shape being transformed
   */
  public int getB() {
    return b;
  }

  /**
   * Gets the appropriate field.
   *
   * @return the rotation amount
   */
  public int getRotation() {
    return rotation;
  }

  /**
   * Sets time.
   *
   * @param time time
   */
  public void setTime(int time) {
    this.time = time;
  }

  /**
   * Sets x.
   *
   * @param x x
   */
  public void setX(int x) {
    this.x = x;
  }

  /**
   * Sets y.
   *
   * @param y y
   */
  public void setY(int y) {
    this.y = y;
  }

  /**
   * Sets width.
   *
   * @param w width
   */
  public void setW(int w) {
    this.w = w;
  }

  /**
   * Sets height.
   *
   * @param h height
   */
  public void setH(int h) {
    this.h = h;
  }

  /**
   * Sets red.
   *
   * @param r red
   */
  public void setR(int r) {
    this.r = r;
  }

  /**
   * Sets green.
   *
   * @param g green
   */
  public void setG(int g) {
    this.g = g;
  }

  /**
   * Sets blue.
   *
   * @param b blue
   */
  public void setB(int b) {
    this.b = b;
  }

  /**
   * Sets rotation.
   *
   * @param rotation rotation
   */
  public void setRotation(int rotation) {
    this.rotation = rotation;
  }
}
