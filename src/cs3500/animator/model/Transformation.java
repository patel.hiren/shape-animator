package cs3500.animator.model;

/**
 * Describes how a shape is transformed over time period (startTime, endTime).
 * If a specific characteristic of the shape doesn't change, its start and end values are equal.
 */
public class Transformation implements ITransformation {

  private int startTime;
  private int endTime;
  private int startX;
  private int endX;
  private int startY;
  private int endY;
  private int startWidth;
  private int endWidth;
  private int startHeight;
  private int endHeight;
  private int startR;
  private int endR;
  private int startG;
  private int endG;
  private int startB;
  private int endB;



  /**
   * Constructs a transformation.
   *
   * @param startTime   the starting time value of the transformation (in ticks)
   * @param endTime     the ending time value of the transformation (in ticks)
   * @param startX      the starting x-position of the shape being transformed
   * @param endX        the ending x-position of the shape ...
   * @param startY      the starting y-position ...
   * @param endY        the ending y-position ...
   * @param startWidth  the starting width ...
   * @param endWidth    the ending width ...
   * @param startHeight the starting height ...
   * @param endHeight   the ending height ...
   * @param startR      the starting color value R ...
   * @param endR        the ending color value R ...
   * @param startG      the starting color value G ...
   * @param endG        the ending color value G ...
   * @param startB      the starting color value B ...
   * @param endB        the ending color value B ...
   */
  public Transformation(int startTime, int endTime, int startX, int endX, int startY, int endY,
                        int startWidth, int endWidth, int startHeight, int endHeight, int startR,
                        int endR, int startG, int endG, int startB, int endB) {
    if (startTime < 0 || endTime < 0 || startR < 0 || endR < 0 || startG < 0 || endG < 0 ||
            startB < 0 || endB < 0 || startR > 255 || endR > 255 || startG > 255 || endG > 255 ||
            startB > 255 || endB > 255 || startTime > endTime) {
      throw new IllegalArgumentException("Time cannot be negative and cannot decrease, " +
              "and Color has to be between 0 and 255");
    }
    this.startTime = startTime;
    this.endTime = endTime;
    this.startX = startX;
    this.endX = endX;
    this.startY = startY;
    this.endY = endY;
    this.startWidth = startWidth;
    this.endWidth = endWidth;
    this.startHeight = startHeight;
    this.endHeight = endHeight;
    this.startR = startR;
    this.endR = endR;
    this.startG = startG;
    this.endG = endG;
    this.startB = startB;
    this.endB = endB;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting time of the transformation (in ticks)
   */
  public int getStartTime() {
    return startTime;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending time of the transformation (in ticks)
   */
  public int getEndTime() {
    return endTime;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting x-position of the shape being transformed
   */
  public int getStartX() {
    return startX;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending x-position of the shape being transformed
   */
  public int getEndX() {
    return endX;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting y-position of the shape being transformed
   */
  public int getStartY() {
    return startY;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending y-position of the shape being transformed
   */
  public int getEndY() {
    return endY;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting width of the shape being transformed
   */
  public int getStartWidth() {
    return startWidth;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending width of the shape being transformed
   */
  public int getEndWidth() {
    return endWidth;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting height of the shape being transformed
   */
  public int getStartHeight() {
    return startHeight;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending height of the shape being transformed
   */
  public int getEndHeight() {
    return endHeight;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting color value R of the shape being transformed
   */
  public int getStartR() {
    return startR;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending color value R of the shape being transformed
   */
  public int getEndR() {
    return endR;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting color value G of the shape being transformed
   */
  public int getStartG() {
    return startG;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending color value G of the shape being transformed
   */
  public int getEndG() {
    return endG;
  }

  /**
   * Gets the appropriate field.
   *
   * @return starting color value B of the shape being transformed
   */
  public int getStartB() {
    return startB;
  }

  /**
   * Gets the appropriate field.
   *
   * @return ending color value B of the shape being transformed
   */
  public int getEndB() {
    return endB;
  }

  /**
   * Gets the starting keyframe of this transformation.
   *
   * @return start keyframe
   */
  public Keyframe getStartFrame() {
    return new Keyframe(startTime, startX, startY, startWidth, startHeight, startR, startG, startB);
  }

  /**
   * Gets the ending keyframe of this transformation.
   *
   * @return end keyframe
   */
  public Keyframe getEndFrame() {
    return new Keyframe(endTime, endX, endY, endWidth, endHeight, endR, endG, endB);
  }
}