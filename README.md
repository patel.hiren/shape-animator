To run this program, you will have to install the packages. It is easy to do 
using IntelliJ by opening my project and right clicking on everything that is 
red and importing the packages.

Once everything is installed, you will want to go to /src/cs3500/animator and 
run the Main function in the file MainAnimation.java. By default, the following 
arguments are loaded: "-in shape-animator/buildings.txt -view edit". But you 
can change this to any file that is in the main directory my changing the text 
file. Your options for -out are visual, edit, SVG, and text. You can also input 
parameter "-speed and -out".